import {ADD_TODO, TOGGLE_TODO, SET_VISIBILITY_FILTER, VisibilityFilters} from './constants'
import {combineReducers} from 'redux'

const initialState = {
  visibilityFilter: VisibilityFilters.SHOW_ALL,
  todos: []
}

function todos(state = [], action) {
  switch (action.type) {
    case ADD_TODO:
      return {
        ...state,
        todos: [
          ...state.todos,
          {
            text: action.text,
            completed: false
          }
        ]
      }
    case TOGGLE_TODO:
      return {
        ...state,
        todos: [
          state.todos.map((todo, index) => {
            if (index === action.index) {
              return {
                ...todo,
                completed: !todo.completed
              }
            }
            return todo
          })
        ]
      }
  }
}

function todoApp(state = initialState, action) {
  switch (action.type) {
    case ADD_TODO:
      return todos(state, action)
    case TOGGLE_TODO:
      return todos(state, action)
    case SET_VISIBILITY_FILTER:
      return {
        ...state,
        visibilityFilter: action.filter
      }
    default:
      state
  }

}